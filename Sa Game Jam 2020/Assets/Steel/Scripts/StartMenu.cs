﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class StartMenu : MonoBehaviour {

    [SerializeField] GameObject Settingsui;
    [SerializeField] GameObject controlsui;
    [SerializeField] GameObject mainui;
    [SerializeField] VideoPlayer cutscene;
    [SerializeField] GameObject skipbtn;

    private void Start () {
        Settingsui.SetActive (false);
        controlsui.SetActive (false);
        skipbtn.SetActive (false);
    }
    public void startgame () {
        StartCoroutine (gamestarter ());
    }

    public void controls () {
        controlsui.SetActive (true);
    }
    public void menfromcontrols () {
        controlsui.SetActive (false);
    }
    public void settings () {
        Settingsui.SetActive (true);
    }
    public void menfromset () {
        Settingsui.SetActive (false);
    }

    public void Quit () {
        Application.Quit ();
    }
    IEnumerator gamestarter () {
        mainui.SetActive (false);
        cutscene.Play ();
        skipbtn.SetActive (true);
        yield return new WaitForSeconds (140f);
        SceneManager.LoadScene (1);
    }

    public void skipper () {
        SceneManager.LoadScene (1);
    }
}